import React, { ReactNode } from "react"
import { createContext, useContext, useEffect, useState } from "react"
import { useRouter } from "next/router"

export function useRouteIsChangingCb() {
  const [routeIsChanging, setRouteIsChanging] = useState(false);
  const router = useRouter()
  useEffect(() => {
    const handleStart = (url: string) => {
      if (url !== router.asPath) {
        setRouteIsChanging(true)
      }
    }
    const handleComplete = () => setRouteIsChanging(false)

    router.events.on('routeChangeStart', handleStart)
    router.events.on('routeChangeComplete', handleComplete)
    router.events.on('routeChangeError', handleComplete)

    return () => {
      router.events.off('routeChangeStart', handleStart)
      router.events.off('routeChangeComplete', handleComplete)
      router.events.off('routeChangeError', handleComplete)
    }
  }, [router])
  return routeIsChanging
}

export const RouteIsChangingCtx = createContext(false)

export function WithRouteIsChanging({ children }: { children: ReactNode }) {
  const routeIsChanging = useRouteIsChangingCb()
  return <RouteIsChangingCtx.Provider value={routeIsChanging}>
    {children}
  </RouteIsChangingCtx.Provider>
}

export function useRouteIsChanging() {
  return useContext(RouteIsChangingCtx)
}
