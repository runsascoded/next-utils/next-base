import configModule from "next/config";
import { NextConfig } from "next";

// TODO: debug why `next/config` gets loaded on client as an object containing a `default` member.
const getConfig = ((configModule as any)['default'] || configModule) as () => NextConfig

export default getConfig
