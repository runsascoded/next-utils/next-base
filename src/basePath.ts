import getConfig from "./config";

/**
 * Get Next.js config basePath (URL path under which the site is deployed, for relative / intra-site linking).
 */
export default function getBasePath() {
    const { publicRuntimeConfig: config } = getConfig()
    if (!config) return ""
    const { basePath = "" } = config
    return basePath
}
