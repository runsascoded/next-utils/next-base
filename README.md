# @rdub/next-base
Misc utils for [Next.js] `<Link>`, `<Head>`, `<Nav>`, `router`, and configs

<a href="https://npmjs.org/package/@rdub/next-base" title="View @rdub/next-base on NPM"><img src="https://img.shields.io/npm/v/@rdub/next-base.svg" alt="@rdub/next-base NPM version" /></a>

- [a](src/a.tsx): link component that uses Next.js &lt;Link&gt; for same-origin URLs and `<a target="_blank">` for external URLs.
- [basePath](src/basePath.ts): `getBasePath` helper for Next.js.
- [config](src/config.ts), [basePath](src/basePath.ts): `next/config` accessors.
- [head](src/head.tsx): `next/head` wrapper, populates og tags.
- [route-is-changing](src/route-is-changing.tsx): `useRouteIsChanging` hook, useful for invalidating queries / displaying loading spinners during `router.push` transitions.

Used by:
- [crashes.hudcostreets.org](https://crashes.hudcostreets.org/)
- [ctbk.dev](https://ctbk.dev/)
- [neighbor-ryan.org](https://neighbor-ryan.org/)
- [@rdub/next-leaflet](https://npmjs.org/package/@rdub/next-leaflet)
- [@rdub/next-markdown](https://npmjs.org/package/@rdub/next-markdown)
- [@rdub/next-params](https://npmjs.org/package/@rdub/next-params)
- [@rdub/next-plotly](https://npmjs.org/package/@rdub/next-plotly)


[Next.js]: https://nextjs.org/
